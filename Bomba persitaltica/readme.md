# Bomba Persitaltica #

## Descripción ##
En este enlace http://biohackacademy.github.io/biofactory/class/8-pumps/ hay un buen punto de partida para la construcción de un perfusor

Este otro modelo es también muy interesante: http://2017.igem.org/Team:Aachen/Hardware

## Hardware ##
* Arduino o un microprocesador similar.
* Motor
* Tubo OD 8mm

## Requerimientos funcionales ##
* Bombeo continuo
* Resolución de 1 ml
* Dirección hacia adelante / hacia atrás
* Velocidad ajustable

## Agradable tener ##
* Dos botones
* Codificador rotatorio
* Pantalla LCD I2C 16x2

## Como testear el bomba persitaltica


