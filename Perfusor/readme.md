# Perfusor #

## Descripción ##
En este enlace http://biohackacademy.github.io/biofactory/class/8-pumps/ hay un buen punto de partida para la construcción de un perfusor

Este otro enlace también es otro perfecto ejemplo: https://github.com/gniezen/openpump

## Hardware ##
* Arduino o un microprocesador similar
* Motor paso a paso
* Jeringa 50-60 ml
[Jeringuilla](https://gitlab.com/coronavirusmakers/ecmo/-/raw/master/images/jeringuilla.jpeg "Jeringuilla de 50-60ml")
[Datos y tamaños de las jeringuillas](https://gitlab.com/coronavirusmakers/ecmo/-/raw/master/files/Syringe-Selection-Guide.pdf "Datos de las jeringuillas")

## Requerimientos funcionales ##
* Resolución de 0.1 mL
* Velocidad ajustable
* Posibilidad de hacer una embolada puntual de x mL

### Agradable tener ###
* Indicación de estado
* Pantalla LCD I2C 16x2
* Dos botones
* Codificador rotatorio

## Como testear el perfusor
